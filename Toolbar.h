#ifndef TOOLBAR_H
#define TOOLBAR_H

#include "Rectangle.h"
#include "Texture.h"

enum Tool {PENCIL, ERASER, SQUARE, MOUSE};

class Toolbar {
    Rectangle area;
    Texture mouseButton;
    Texture pencilButton;
    Texture eraserButton;
    Texture squareButton;
    Tool selectedTool;

    void deselectAll(){
        mouseButton.selected = false;
        pencilButton.selected = false;
        eraserButton.selected = false;
        squareButton.selected = false;
    }

public:
    Toolbar(){
        area = Rectangle(-1.0f, 1.0f, 0.2f, 2.0f, Color(0.8f, 0.8f, 0.8f));
        mouseButton = Texture("assets/mouse.png", -1.0f, 1.0f, 0.2f, 0.2f);
        pencilButton = Texture("assets/pencil.png", -1.0f, 0.8f, 0.2f, 0.2f);
        eraserButton = Texture("assets/eraser.png", -1.0f, 0.6f, 0.2f, 0.2f);
        squareButton = Texture("assets/square.png", -1.0f, 0.4f, 0.2f, 0.2f);

        selectedTool = PENCIL;
        pencilButton.selected = true;
    }

    // getters
    Tool getSelectedTool() {
        return selectedTool;
    }

    // setters
    void selectMouse() {
        deselectAll();
        mouseButton.selected = true;
        selectedTool = MOUSE;
    }

    void selectPencil() {
        deselectAll();
        pencilButton.selected = true;
        selectedTool = PENCIL;
    }

    void selectEraser() {
        deselectAll();
        eraserButton.selected = true;
        selectedTool = ERASER;
    }

    void selectSquare() {
        deselectAll();
        squareButton.selected = true;
        selectedTool = SQUARE;
    }

    void handleMouseClick(float x, float y){
        if (mouseButton.contains(x, y)){
            selectMouse();
        }
        else if (pencilButton.contains(x, y)){
            selectPencil();
        }
        else if (eraserButton.contains(x, y)){
            selectEraser();
        }
        else if (squareButton.contains(x, y)){
            selectSquare();
        }
    }

    void draw(){
        area.draw();
        mouseButton.draw();
        pencilButton.draw();
        eraserButton.draw();
        squareButton.draw();
    }

    bool contains(float x, float y){
        return area.contains(x, y);
    }
};

#endif