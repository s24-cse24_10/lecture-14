#ifndef CANVAS_H
#define CANVAS_H

#include "Rectangle.h"
#include "Toolbar.h"
#include "Point.h"

struct Canvas {
    Rectangle area;

    Point points[1000];
    int pCounter;

    Rectangle squares[1000];
    int sCounter;

    Canvas(){
        area = Rectangle(-0.8f, 1.0f, 1.8f, 1.8f, Color(1.0f, 1.0f, 1.0f));
        pCounter = 0;
        sCounter = 0;
    }

    void handleMouseClick(float x, float y, Tool tool, Color color){
        if (tool == PENCIL){
            points[pCounter] = Point(x, y, color);
            pCounter++;
        }
        else if (tool == ERASER){
            points[pCounter] = Point(x, y, Color(1.0f, 1.0f, 1.0f), 20.0f);
            pCounter++;
        }
        else if (tool == SQUARE){
            squares[sCounter] = Rectangle(x, y, 0.2f, 0.2f, color);
            sCounter++;
        }
    }

    void draw(){
        area.draw();

        for (int i = 0; i < pCounter; i++){
            points[i].draw();
        }

        for (int i = 0; i < sCounter; i++){
            squares[i].draw();
        }
    }

    bool contains(float x, float y){
        return area.contains(x, y);
    }
};

#endif